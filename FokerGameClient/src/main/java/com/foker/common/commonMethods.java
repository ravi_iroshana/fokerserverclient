/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.common;

import com.foker.controller.HomeController;
import com.foker.entity.CardDetail;
import com.foker.entity.Player;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;

/**
 *
 * @author Ravindu
 */
public class commonMethods {
    private static HomeController homeController;
        
    public static ArrayList<CardDetail> lstCrdDetails = new ArrayList<>();
    
    private static final List<Player> ALL_PLAYERS = new ArrayList<Player>();
    
    public static List<Player> getPlayers(){
        return ALL_PLAYERS;
    }
    
    public static void setPlayerJoin(String username, ObservableList<String> oblst){
        oblst.add(0, username+"Join");
        
        if(oblst.size() >= 2 && commonData.isWaiting){
            try {
                homeController.loadsplash();
            } catch (IOException ex) {
                Logger.getLogger(commonMethods.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
    
    }
}
