/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.common;

/**
 *
 * @author Ravindu
 */
public class commonData {
    
    public static String USERNAME;
    public static PlayerStatus ROUND_COMPLETE = null;
    public static final PlayerStatus PLAYER_STATUS = PlayerStatus.PLAYING;
    public static boolean isWaiting = true;
    public static String CURRENT_ROUND = "1";
    public static int ROUND = 1;
    public static int TOTAL = 0;
    
    public final static String IP = "http://localhost:8080/FokerGameServer/WebResources/";
    public final static String GET = "GET";
    public final static String POST = "POST";
    
    public final static String PLAYER_SERVICE = "PlayerService/";
    public final static String REGISTER_PLAYER = "registerPlayer";
    public final static String LOGIN_PLAYER = "loginPlayer";
    public final static String GET_JOIN_PLAYER = "getJoinPlayer";
    public final static String GET_PLAYER_INFO = "GetPlayerinfo";
    
    public final static String CARD_SERVICE = "CardServices/";
    public final static String GET_INITIAL_CARD = "getInitialCard";
    public final static String GET_REMAINING_CARD ="getRemainingCards";
    
    public final static String BROADCAST_NOTIFICATION_SERVICE  = "BroadcastNotificationService/";
    public final static String GET_BROADCAST_JOIN_PLAYER = "getBroadcastJoinPlayer";
    public final static String LISTEN_JOIN_PLAYER = "listenJoinPlayer";
    
    public final static String SCORE_SERVICES = "ScoreServices/";
    public final static String RECEIVE_HAND = "receiveHand";
}
