/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.controller;

import com.foker.common.Validation;
import com.foker.services.PlayerServices;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author thari
 */
public class SignupController implements Initializable {

    private final Validation validation = new Validation();

    @FXML
    private AnchorPane root;
    @FXML
    private JFXTextField userName;
    @FXML
    private JFXTextField email;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXPasswordField confermpass;
    @FXML
    private JFXButton btn_login;
    @FXML
    private JFXButton btn_cancle;
    @FXML
    private Label msgLabel;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void signup(ActionEvent event) throws IOException {
        String result;

        if (validation.validateNull(userName.getText()) && validation.validateNull(password.getText()) && validation.validateNull(email.getText())) {

            if (validation.validateNull(confermpass.getText()) && password.getText().equals(confermpass.getText())) {
                result = PlayerServices.registerPlayer(userName.getText(), password.getText(), email.getText());
                if (result.equals("Success")) {
                    AnchorPane apane = FXMLLoader.load(getClass().getResource("/fxml/Login.fxml"));
                    fadein(apane);
                    root.getChildren().setAll(apane);

                } else {
                    msgLabel.setText("User already exsist");                    
                }

            } else {
                msgLabel.setText("Password and Confirm Password should be equal");                
            }
        } else {
            msgLabel.setText("All fields are Required");            
        }
    }

    @FXML
    private void clear(ActionEvent event) {
        userName.setText("");
        email.setText("");
        password.setText("");
        confermpass.setText("");
    }

    private void fadein(Node e) {
        FadeTransition fadein = new FadeTransition(Duration.seconds(3), e);
        fadein.setFromValue(0);
        fadein.setToValue(1);
        fadein.setCycleCount(1);
        fadein.play();
    }

    @FXML
    private void back(ActionEvent event) throws IOException {
        AnchorPane apane = FXMLLoader.load(getClass().getResource("/fxml/Login.fxml"));
        fadein(apane);
        root.getChildren().setAll(apane);
    }

}
