/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.controller;

import com.foker.common.Validation;
import com.foker.services.PlayerServices;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.swing.JOptionPane;
import com.foker.common.Validation;
import com.foker.common.commonData;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author thari
 */
public class LoginController implements Initializable {

    private final Validation validation = new Validation();
    
    @FXML
    private AnchorPane root;
    @FXML
    private Hyperlink signup;
    @FXML
    private JFXTextField userName;
    @FXML
    private JFXPasswordField passowrd;
    @FXML
    private JFXButton btn_login;
    @FXML
    private JFXButton btn_cancle;
    @FXML
    private JFXButton btn_exit;
    @FXML
    private Label msgLabel;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    private void loadsplash() throws IOException{
        try{
            Validation.isSplashLoad = true;
            StackPane spane = FXMLLoader.load(getClass().getResource("/fxml/Splash.fxml"));
            root.getChildren().setAll(spane);
            
            FadeTransition fadein = new FadeTransition(Duration.seconds(3),spane);
            fadein.setFromValue(0);
            fadein.setToValue(1);
            fadein.setCycleCount(1);
            
            FadeTransition fadeout = new FadeTransition(Duration.seconds(3),spane);
            fadeout.setFromValue(1);
            fadeout.setToValue(0);
            fadeout.setCycleCount(1);
            
            fadein.play();            
            
            fadein.setOnFinished(e->{
                fadeout.play();
            });
            fadeout.setOnFinished(e->{
                try {
                    AnchorPane main = FXMLLoader.load(getClass().getResource("/fxml/Home.fxml"));
                    fadein(main);
                    root.getChildren().setAll(main);
                } catch (IOException ex) {
                    Logger.getLogger(com.foker.controller.LoginController.class.getName()).log(Level.SEVERE, null, ex);
                }            
            });
        }catch(IOException ex){
            
        }       
    }

    @FXML
    private void login(ActionEvent event) {
        String result = null;
        if(validation.validateNull(userName.getText()) && validation.validateNull(passowrd.getText())){
            result = PlayerServices.loginPlayer(userName.getText(), passowrd.getText());
            
            if(result.equals("Success")){
                try {
                    commonData.USERNAME = userName.getText();
                    loadsplash();
                } catch (IOException ex) {
                    Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
                }
            
            }else{
                msgLabel.setText("User need to Signup");                
            }
               
        }else{
            msgLabel.setText("Username and Password should be required");            
        }

    }

    @FXML
    private void clear(ActionEvent event) {
        try{
        Notifications notif = Notifications.create()
                .title("hhaha")
                .text("jbxhs k cxzxx")
                .graphic(null)
                .hideAfter(Duration.seconds(5))
                .position(Pos.TOP_LEFT)
                .onAction((ActionEvent event1) -> {
                    System.out.println("hiiiiiiiiiiiii");
        });
        
        notif.showConfirm();
        }catch(Exception e){
            System.out.println(e);
        }
    }

    @FXML
    private void exit(ActionEvent event) {
        Stage stage = (Stage) btn_exit.getScene().getWindow();
            stage.close();
            //removePlayer(CommonData.username);
            System.exit(0);
    }

    @FXML
    private void loadsignup(ActionEvent event) throws IOException {
        AnchorPane apane = FXMLLoader.load(getClass().getResource("/fxml/Signup.fxml"));
        fadein(apane);
        root.getChildren().setAll(apane);
    }
    private void fadein(Node e){
        FadeTransition fadein = new FadeTransition(Duration.seconds(3),e);
            fadein.setFromValue(0);
            fadein.setToValue(1);
            fadein.setCycleCount(1);
            fadein.play();
    }
}
