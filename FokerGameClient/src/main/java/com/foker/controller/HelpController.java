/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author thari
 */
public class HelpController implements Initializable {

    @FXML
    private AnchorPane root;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    private void fadein(Node e) {
        FadeTransition fadein = new FadeTransition(Duration.seconds(3), e);
        fadein.setFromValue(0);
        fadein.setToValue(1);
        fadein.setCycleCount(1);
        fadein.play();
    }
    @FXML
    private void back(ActionEvent event) throws IOException {
        AnchorPane apane = FXMLLoader.load(getClass().getResource("/fxml/Home.fxml"));
        fadein(apane);
        root.getChildren().setAll(apane);
    }
    
}
