/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.controller;

import com.foker.common.Validation;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author Ravindu
 */
public class SideMenuController implements Initializable {

    @FXML
    private JFXButton sideMenuPlay;
    @FXML
    private JFXButton sideMenuProfile;
    @FXML
    private JFXButton sideMenuHelp;
    @FXML
    private JFXButton sideMenuAbout;
    @FXML
    private JFXButton sideMenuExit;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void Exit(ActionEvent evt) {
        Stage stage = (Stage) sideMenuExit.getScene().getWindow();
        stage.close();
        System.exit(0);
    }
    
    @FXML
    private void changeScreen(ActionEvent event) throws IOException {
        JFXButton btn = (JFXButton) event.getSource();
        AnchorPane main;
        switch(btn.getText())
        {
            case "Play": main = FXMLLoader.load(getClass().getResource("/fxml/Gameboard.fxml"));
                    HomeController.rootP.getChildren().setAll(main);
                    break;
            case "Profile": main = FXMLLoader.load(getClass().getResource("/fxml/Profile.fxml"));
                    HomeController.rootP.getChildren().setAll(main);
                    break;
            case "Help":main = FXMLLoader.load(getClass().getResource("/fxml/Help.fxml"));
                    HomeController.rootP.getChildren().setAll(main);
                    break;
            case "About":main = FXMLLoader.load(getClass().getResource("/fxml/About.fxml"));
                    HomeController.rootP.getChildren().setAll(main);
                    break;
        }
    }
    
    private void Play(ActionEvent evt){
        try {
            loadsplash();
        } catch (IOException ex) {
            Logger.getLogger(SideMenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadsplash() throws IOException{
        try{
            Validation.isSplashLoad = true;
            StackPane spane = FXMLLoader.load(getClass().getResource("/fxml/Splash.fxml"));
            //Apane.getChildren().setAll(spane);
            
            FadeTransition fadein = new FadeTransition(Duration.seconds(3),spane);
            fadein.setFromValue(0);
            fadein.setToValue(1);
            fadein.setCycleCount(1);
            
            FadeTransition fadeout = new FadeTransition(Duration.seconds(3),spane);
            fadeout.setFromValue(1);
            fadeout.setToValue(0);
            fadeout.setCycleCount(1);
            
            fadein.play();            
            
            fadein.setOnFinished(e->{
                fadeout.play();
            });
            fadeout.setOnFinished(e->{
                try {
                    AnchorPane main = FXMLLoader.load(getClass().getResource("/fxml/Gameboard.fxml"));
                    fadein(main);
                    //Apane.getChildren().setAll(main);
                } catch (IOException ex) {
                    Logger.getLogger(com.foker.controller.HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }            
            });
        }catch(IOException ex){
            
        }       
    }
    
    private void fadein(Node e){
        FadeTransition fadein = new FadeTransition(Duration.seconds(3),e);
            fadein.setFromValue(0);
            fadein.setToValue(1);
            fadein.setCycleCount(1);
            fadein.play();
    }    

}
