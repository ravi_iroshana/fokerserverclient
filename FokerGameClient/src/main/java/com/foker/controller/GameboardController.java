/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.controller;

import com.foker.common.commonData;
import static com.foker.common.commonData.TOTAL;
import static com.foker.common.commonMethods.lstCrdDetails;
import com.foker.entity.CardDetail;
import com.foker.entity.Player;
import static com.foker.services.CardService.getInitialCard;
import static com.foker.services.CardService.getRemaningCard;
import static com.foker.services.CardService.reciveResult;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author thari
 */
public class GameboardController implements Initializable {

    @FXML
    private Pane P5;
    @FXML
    private Pane P4;
    @FXML
    private Pane P3;
    @FXML
    private Pane P2;
    @FXML
    private Pane P1;

    private int cards = 1;
    private String selectp1 = "", selectp2 = "", selectp3 = "", selectp4 = "", selectp5 = "";
    private int[] ypoint = new int[]{350, 150, -170, -350, -150, 170};
    private int[] xpoint = new int[]{50, -450, -450, 100, 600, 600};

    @FXML
    private ImageView animatecardset;
    @FXML
    private ImageView animatecardget;
    @FXML
    private Pane P51;
    @FXML
    private Pane P41;
    @FXML
    private Pane P31;
    @FXML
    private Pane P21;
    @FXML
    private Pane P11;
    @FXML
    private Pane P511;
    @FXML
    private Pane P411;
    @FXML
    private Pane P311;
    @FXML
    private Pane P211;
    @FXML
    private Pane P111;
    @FXML
    private Pane P512;
    @FXML
    private Pane P412;
    @FXML
    private Pane P312;
    @FXML
    private Pane P212;
    @FXML
    private Pane P112;
    @FXML
    private Pane P513;
    @FXML
    private Pane P413;
    @FXML
    private Pane P313;
    @FXML
    private Pane P213;
    @FXML
    private Pane P113;
    @FXML
    private Pane P5131;
    @FXML
    private Pane P4131;
    @FXML
    private Pane P3131;
    @FXML
    private Pane P2131;
    @FXML
    private Pane P1131;
    @FXML
    private ImageView p1card5;
    @FXML
    private ImageView p1card4;
    @FXML
    private ImageView p1card3;
    @FXML
    private ImageView p1card2;
    @FXML
    private ImageView p1card1;
    @FXML
    private JFXButton btnView;
    @FXML
    private JFXButton btnGo;
    @FXML
    private JFXButton btnCall;
    @FXML
    private JFXButton btnFold;
    @FXML
    private JFXButton btnChange;
    @FXML
    private ImageView rootimage1;
    @FXML
    private ImageView rootimage;
    @FXML
    private Pane PaneScore;
    @FXML
    private Label PlayerName;
    @FXML
    private Label newbtn;
    @FXML
    private Label Roundwin;
    @FXML
    private Label Ranking;
    @FXML
    private Label TotalPoint;
    @FXML
    private Label btnContandEnd;
    @FXML
    private AnchorPane root;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnCall.setVisible(false);
        btnFold.setVisible(false);
        btnChange.setVisible(false);
        PaneScore.setVisible(false);
        btnView.setVisible(false);
    }

    @FXML
    private void MakeSelectionPC1(MouseEvent event) {
        if ("PC1".equals(selectp1)) {
            deselect(P1, 40, 0);
            selectp1 = "";
        } else if (cards <= 3) {
            System.out.println("awa");
            System.out.println(cards);
            select(P1, -40, 0);
            //selected[cards] = "pc5";
            selectp1 = "PC1";
        } else {
            System.out.println(cards);
        }
    }

    private void select(Node e, double y, double x) {
        TranslateTransition translate = new TranslateTransition();
        translate.setDuration(Duration.seconds(1));
        translate.setNode(e);

        translate.setByY(y);
        translate.setByX(x);
        translate.play();
        translate.setOnFinished((e1) -> {
            cards++;
        });
        
        System.out.println("                              "+e.getUserData());       
    }

    private void deselect(Node e, double y, double x) {
        TranslateTransition translate = new TranslateTransition();
        translate.setDuration(Duration.seconds(1));
        translate.setNode(e);

        translate.setByY(y);
        translate.setByX(x);
        translate.play();
        translate.setOnFinished((e1) -> {
            cards--;
        });
    }

    @FXML
    private void MakeSelectionPC5(MouseEvent event) {
        if ("PC5".equals(selectp5)) {
            deselect(P5, 40, 0);
            selectp5 = "";
        } else if (cards <= 3) {
            System.out.println("awa");
            System.out.println(cards);
            select(P5, -40, 0);
            //selected[cards] = "pc5";
            selectp5 = "PC5";
            System.out.println(cards);
        } else {
            System.out.println(cards);
        }
    }

    @FXML
    private void MakeSelectionPC4(MouseEvent event) {
        if ("PC4".equals(selectp4)) {
            deselect(P4, 40, 0);
            selectp4 = "";
        } else if (cards <= 3) {
            System.out.println("awa");
            System.out.println(cards);
            select(P4, -40, 0);
            //selected[cards] = "pc5";
            selectp4 = "PC4";
            System.out.println(cards);
        } else {
            System.out.println(cards);
        }
    }

    @FXML
    private void MakeSelectionPC3(MouseEvent event) {
        if ("PC3".equals(selectp3)) {
            deselect(P3, 40, 0);
            selectp3 = "";
        } else if (cards <= 3) {
            System.out.println("awa");
            System.out.println(cards);
            select(P3, -40, 0);
            //selected[cards] = "pc5";
            selectp3 = "PC3";
        } else {
            System.out.println(cards);
        }
    }

    @FXML
    private void MakeSelectionPC2(MouseEvent event) {
        if ("PC2".equals(selectp2)) {
            deselect(P2, 40, 0);
            selectp2 = "";
        } else if (cards <= 3) {
            System.out.println("awa");
            System.out.println(cards);
            select(P2, -40, 0);
            //selected[cards] = "pc5";
            selectp2 = "PC2";
        } else {
            System.out.println(cards);
        }
    }

    private void fade(Node e, double fvalue, double tvalue) {
        FadeTransition fadein = new FadeTransition(Duration.seconds(1), e);
        fadein.setFromValue(fvalue);
        fadein.setToValue(tvalue);
    }

    int click = 0;

    @FXML
    private void GiveCards(ActionEvent event) throws InterruptedException {
        /*if (click < 5) {
            cardsgo(ypoint[click], xpoint[click]);
            click++;

        } else if (click == 5) {
            cardsgo(ypoint[click], xpoint[click]);
            click = 0;
        }*/

        for (int i = 0; i <= 5; i++) {
            cardsgo(ypoint[click], xpoint[click]);
        }
        btnView.setVisible(true);
        btnGo.setVisible(false);        
    }

    private void cardsgo(int yvalue, int xvalue) {

        animatecardset.setOpacity(1);
        TranslateTransition translate = new TranslateTransition();
        translate.setDuration(Duration.seconds(1));
        translate.setNode(animatecardset);

        translate.setByY(yvalue);
        translate.setByX(xvalue);
        translate.play();
        translate.setOnFinished((ex) -> {
            cardback(-yvalue, -xvalue);
        });
    }

    private void cardback(int yvalue, int xvalue) {
        System.out.println("giya");
        TranslateTransition translate = new TranslateTransition();
        translate.setDuration(Duration.seconds(0.1));
        translate.setNode(animatecardset);
        animatecardset.setOpacity(0);
        translate.setByY(yvalue);
        translate.setByX(xvalue);
        translate.play();
    }

    boolean isInitial = true;

    @FXML
    private void setInitialCards(ActionEvent evt) {
        if (isInitial) {
            int i = 0;
            getInitialCard();
            for (CardDetail crd : lstCrdDetails) {
                if (i == 0) {
                    Image image = new Image(getClass().getResourceAsStream(crd.getImgPath()));
                    p1card1.setImage(image);
                    i++;
                } else if (i == 1) {
                    Image image = new Image(crd.getImgPath());
                    p1card2.setImage(image);
                    i++;
                }
            }
            isInitial = false;
            btnView.setText("Click");
        } else {
            setRemainingCard();
            btnView.setVisible(false);
            btnCall.setVisible(true);
            btnFold.setVisible(true);
            btnChange.setVisible(true);
            isInitial = true;
        }
    }

    private void setRemainingCard() {
        ArrayList<CardDetail> lst = getRemaningCard();
        int i = 0;
        for (CardDetail crd : lst) {
            lstCrdDetails.add(crd);
            if (i == 0) {
                Image image = new Image(crd.getImgPath());
                p1card3.setImage(image);
                i++;
            } else if (i == 1) {
                Image image = new Image(crd.getImgPath());
                p1card4.setImage(image);
                i++;
            } else if (i == 2) {
                Image image = new Image(crd.getImgPath());
                p1card5.setImage(image);
                i++;
            }
        }
    }

    @FXML
    private void sendCards(ActionEvent evt) {
        
        Player result = reciveResult();
        
        if(result != null) {
            if(result.getPosition() == 0){
                PlayerName.setText(commonData.USERNAME);
                Roundwin.setText(commonData.ROUND + "/6");
                TotalPoint.setText(String.valueOf(TOTAL));
                btnContandEnd.setText("Continue");
                PaneScore.setVisible(true);            
            }else{
                btnContandEnd.setText("END");            
            }
            
        }    

    }
    
    @FXML
    private void newGame() throws IOException{
        PaneScore.setVisible(false);
        AnchorPane main = FXMLLoader.load(getClass().getResource("/fxml/Home.fxml"));
        root.getChildren().setAll(main);    
    }

    @FXML
    private void continueGame(MouseEvent event) {
        commonData.ROUND++;
        btnCall.setVisible(false);
        btnFold.setVisible(false);
        btnChange.setVisible(false);
        PaneScore.setVisible(false);
        btnView.setVisible(false);
        
        for (int i = 0; i <= 5; i++) {
            cardsgo(ypoint[click], xpoint[click]);
        }
        btnView.setVisible(true);
        btnGo.setVisible(false);
        
        Image defaultimg = new Image(getClass().getResourceAsStream("/images/keAy7TN.jpg"));
        p1card1.setImage(defaultimg);
        p1card2.setImage(defaultimg);
        p1card3.setImage(defaultimg);
        p1card4.setImage(defaultimg);
        p1card5.setImage(defaultimg);
        
        PaneScore.setVisible(false);
        btnView.setText("View");
 
    }
}
