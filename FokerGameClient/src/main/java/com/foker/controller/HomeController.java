/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.controller;

import com.foker.common.PlayerStatus;
import com.foker.common.Validation;
import com.foker.common.commonData;
import static com.foker.common.commonData.ROUND_COMPLETE;
import com.foker.entity.Player;
import com.foker.services.PlayerServices;
import static com.foker.services.PlayerServices.listenJoinPlayer;
import static com.foker.services.PlayerServices.notificationPlayerJoin;
import static com.foker.services.PlayerServices.setObservable;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.PathTransition;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author thari
 */
public class HomeController implements Initializable {
    
    private List<Player> allOnlinePlayer;
    ObservableList<String> observaleLst = FXCollections.observableArrayList();

    @FXML
    private JFXHamburger hmb;
    @FXML
    private JFXDrawer drawer;
    @FXML
    public AnchorPane Apane;
    
    public static AnchorPane rootP;
    @FXML
    private Pane menu3;
    @FXML
    private ImageView playimg;
    private JFXButton sideMenuExit;
    @FXML
    private Label loginUser;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            rootP = Apane;
            // TODO
            VBox box = FXMLLoader.load(getClass().getResource("/fxml/SideMenu.fxml"));

            drawer.setSidePane(box);
            HamburgerBackArrowBasicTransition animate = new HamburgerBackArrowBasicTransition(hmb);
            animate.setRate(-1);
            hmb.addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {
                animate.setRate(animate.getRate() * -1);
                animate.play();
                if (drawer.isShown()) {
                    drawer.close();
                } else {
                    drawer.open();
                }

            });
        } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        FadeTransition first = new FadeTransition(Duration.seconds(5), playimg);

        RotateTransition rfirst = new RotateTransition(Duration.seconds(5), menu3);
        rfirst.setAutoReverse(false);
        rfirst.setCycleCount(Timeline.INDEFINITE);
        rfirst.setFromAngle(0);
        rfirst.setToAngle(360);
        rfirst.play();

        FadeTransition[] fade = {first};
        for (FadeTransition fadeTransition : fade) {
            fadeTransition.setAutoReverse(false);
            fadeTransition.setFromValue(0);
            fadeTransition.setToValue(1);
            fadeTransition.setCycleCount(1);
        }
        
        //loginUser.setText("UserName = " + commonData.USERNAME);
        
        allOnlinePlayer = PlayerServices.getJoinPlayer();
        setObservable(observaleLst);
        
        Thread thread = new Thread(() -> {
            notificationPlayerJoin();
            if(ROUND_COMPLETE == null){
                listenJoinPlayer();
            }
        });
        
        thread.setDaemon(true);
        thread.start();
    }

    public void fadein(Node e, double fromval) {
        FadeTransition first = new FadeTransition(Duration.seconds(1), e);
        first.setAutoReverse(false);
        first.setCycleCount(1);
        first.setFromValue(fromval);
        first.setToValue(1);
        first.play();
    }

    public void fadeout(Node e, double toval) {
        FadeTransition fadeout = new FadeTransition(Duration.seconds(1), e);
        fadeout.setFromValue(1);
        fadeout.setToValue(toval);
        fadeout.setCycleCount(1);
    }

    public void move(Node e) {
        Path path = new Path();
        path.getElements().add(new MoveTo(0, 100));
        path.getElements().add(new CubicCurveTo(380, 0, 380, 120, 0, 0));
        path.getElements().add(new CubicCurveTo(0, 120, 0, 240, 380, 240));
        PathTransition pathTransition = new PathTransition();
        pathTransition.setDuration(Duration.millis(4000));
        pathTransition.setPath(path);
        pathTransition.setNode(e);
        pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        pathTransition.setCycleCount(Timeline.INDEFINITE);
        pathTransition.setAutoReverse(false);
        pathTransition.play();
    }

    @FXML
    private void fadeon(MouseEvent event) {
        fadein(playimg, 0.7);
    }

    @FXML
    private void fadeoff(MouseEvent event) {
        fadeout(playimg, 0.7);
    }
    
    @FXML
    private void playing(MouseEvent evt){
        try {
            loadsplash(); 
            commonData.isWaiting = true;
        } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    public void loadsplash() throws IOException{
        try{
            Validation.isSplashLoad = true;
            StackPane spane = FXMLLoader.load(getClass().getResource("/fxml/Splash.fxml"));
            Apane.getChildren().setAll(spane);
            
            FadeTransition fadein = new FadeTransition(Duration.seconds(3),spane);
            fadein.setFromValue(0);
            fadein.setToValue(1);
            fadein.setCycleCount(1);
            
            FadeTransition fadeout = new FadeTransition(Duration.seconds(3),spane);
            fadeout.setFromValue(1);
            fadeout.setToValue(0);
            fadeout.setCycleCount(1);
            
            fadein.play();            
            
            fadein.setOnFinished(e->{
                fadeout.play();
            });
            fadeout.setOnFinished(e->{
                try {
                    AnchorPane main = FXMLLoader.load(getClass().getResource("/fxml/Gameboard.fxml"));
                    fadein(main);
                    Apane.getChildren().setAll(main);
                } catch (IOException ex) {
                    Logger.getLogger(com.foker.controller.HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }            
            });
        }catch(IOException ex){
            
        }       
    }
    
    
    private void fadein(Node e){
        FadeTransition fadein = new FadeTransition(Duration.seconds(3),e);
            fadein.setFromValue(0);
            fadein.setToValue(1);
            fadein.setCycleCount(1);
            fadein.play();
    }
    
    private void Exit(ActionEvent evt) {
        Stage stage = (Stage) sideMenuExit.getScene().getWindow();
        stage.close();
        System.exit(0);
    }
}
