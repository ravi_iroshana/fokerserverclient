/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.services;

import com.foker.common.PlayerStatus;
import com.foker.common.commonData;
import static com.foker.common.commonData.BROADCAST_NOTIFICATION_SERVICE;
import static com.foker.common.commonData.GET_BROADCAST_JOIN_PLAYER;
import static com.foker.common.commonData.GET_JOIN_PLAYER;
import static com.foker.common.commonData.GET_PLAYER_INFO;
import static com.foker.common.commonData.LISTEN_JOIN_PLAYER;
import static com.foker.common.commonData.LOGIN_PLAYER;
import static com.foker.common.commonData.PLAYER_SERVICE;
import static com.foker.common.commonData.REGISTER_PLAYER;
import static com.foker.common.commonMethods.setPlayerJoin;
import com.foker.entity.Player;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.media.sse.EventInput;
import org.glassfish.jersey.media.sse.InboundEvent;
import org.glassfish.jersey.media.sse.SseFeature;
import org.json.JSONObject;

/**
 *
 * @author Ravindu
 */
public class PlayerServices {
    
    private static ObservableList<String> lst = null;
    
    public static void setObservable(ObservableList<String> lst){
        PlayerServices.lst = lst;
    }
    
    public static Player getCurrentPlayer(){
        Player player = new Player();
        try{
            URL url = new URL(commonData.IP + PLAYER_SERVICE + GET_PLAYER_INFO + "/" + commonData.USERNAME);
            Client client = Client.create();
            WebResource webRes = client.resource(url.toString());
            
            ClientResponse cliRes = webRes.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            String out = cliRes.getEntity(String.class);
            
            Gson gson = new Gson();
            player = gson.fromJson(out, Player.class);
            JSONObject obj = new JSONObject(out);
            player.setEmail(obj.getString("email"));
            player.setUsername(obj.getString("username"));
            player.setRank(obj.getInt("rank"));
            player.setPoint(obj.getInt("points"));
            
            return player;
        
        }catch(JsonSyntaxException | ClientHandlerException | UniformInterfaceException | MalformedURLException ex){
            Logger.getLogger(PlayerServices.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return player;    
    }
    
    public static void notificationPlayerJoin(){
        try {
            URL url = new URL(commonData.IP + BROADCAST_NOTIFICATION_SERVICE + GET_BROADCAST_JOIN_PLAYER + "/" + commonData.USERNAME);
            Client client = Client.create();
            WebResource webRes = client.resource(url.toString());
            
            ClientResponse cliRes = webRes.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            String out = cliRes.getEntity(String.class);
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(PlayerServices.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void listenJoinPlayer(){
        javax.ws.rs.client.Client client = ClientBuilder.newBuilder().register(SseFeature.class).build();
        WebTarget target = client.target(commonData.IP + BROADCAST_NOTIFICATION_SERVICE + LISTEN_JOIN_PLAYER);
        
        EventInput evtInput = target.request().get(EventInput.class);
        
        while(!evtInput.isClosed()){
            InboundEvent inEvt = evtInput.read();
            
            if(commonData.PLAYER_STATUS ==PlayerStatus.PLAYING){
                break;            
            }
            
            if(inEvt == null){
                break;
            }
            
            Platform.runLater(() -> {
                setPlayerJoin(inEvt.readData(String.class),lst);
            });
        }
    }

    public static String loginPlayer(String userName, String password) {
        String output = null;

        try {            
            URL url = new URL(commonData.IP + PLAYER_SERVICE + LOGIN_PLAYER+ "/" + userName + "/" + password);
            
            output = getData(url);

        } catch (IOException ex) {
            System.out.println(ex);
        } catch (Exception e) {
            System.out.println(e);
        }

        return output;
    }

    public static String registerPlayer(String userName, String password, String email) {
        String output = null;
        try {            
            URL url = new URL(commonData.IP + PLAYER_SERVICE + REGISTER_PLAYER+ "/" + userName + "/" + password + "/" + email);
            output = sendData(userName, password, email, url);
            
        } catch (IOException ex) {
            System.out.println(ex);
        } catch (Exception e) {
            System.out.println(e);
        }

        return output;
    }
    
    public static List<Player> getJoinPlayer(){
        List<Player> lstPlayer = new ArrayList<>();
        
        try{
            URL url = new URL(commonData.IP + PLAYER_SERVICE + GET_JOIN_PLAYER);
            Client client = Client.create();
            WebResource webRes = client.resource(url.toString());
            
            ClientResponse cliRes = webRes.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            String out = cliRes.getEntity(String.class);
            
            Gson gson = new Gson();
            
            lstPlayer = gson.fromJson(out, new TypeToken<ArrayList<Player>>(){}.getType());
            
            return lstPlayer;
        }catch(JsonSyntaxException | ClientHandlerException | UniformInterfaceException | MalformedURLException ex){
            System.out.println(ex);
        }
        
        return lstPlayer;
    }

    private static String sendData(String userName, String password, String email, URL connection) {
        try {
            Client client = Client.create();
            WebResource webRes = client.resource(connection.toString());            
            ClientResponse cliRes = webRes.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            String out = cliRes.getEntity(String.class);
            
            if(cliRes.getStatus() != 200)
            {out = null;}
            
            System.out.println(out);
            return out;
            
        } catch (ClientHandlerException | UniformInterfaceException e) {
            System.out.println(e);
        }
        return null;
    }

    private static String getData(URL connection) throws IOException {

        String get = null;
        try {
            Client client = Client.create();
            WebResource res = client.resource(connection.toString());
            ClientResponse cliRes = res.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            get = cliRes.getEntity(String.class);
            return get;
        } catch (ClientHandlerException | UniformInterfaceException ex) {
            System.out.println(ex);
        }

        return get;
    }
}
