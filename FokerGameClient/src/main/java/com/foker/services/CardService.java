/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.services;

import com.foker.common.commonData;
import static com.foker.common.commonData.CARD_SERVICE;
import static com.foker.common.commonData.GET_INITIAL_CARD;
import static com.foker.common.commonData.GET_REMAINING_CARD;
import static com.foker.common.commonData.RECEIVE_HAND;
import static com.foker.common.commonData.SCORE_SERVICES;
import static com.foker.common.commonData.TOTAL;
import static com.foker.common.commonMethods.lstCrdDetails;
import com.foker.entity.CardDetail;
import com.foker.entity.Cards;
import com.foker.entity.Player;
import com.foker.entity.RoundResult;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import org.json.JSONObject;

/**
 *
 * @author Ravindu
 */
public class CardService {

    public static ArrayList<CardDetail> getInitialCard() {

        try {
            URL url = new URL(commonData.IP + CARD_SERVICE + GET_INITIAL_CARD);
            String output = getData(url);
            Gson gson = new Gson();

            lstCrdDetails = gson.fromJson(output, new TypeToken<ArrayList<CardDetail>>() {
            }.getType());
        } catch (IOException | JsonSyntaxException e) {
            System.out.println(e);
        }

        return lstCrdDetails;
    }

    public static ArrayList<CardDetail> getRemaningCard() {
        try {

            ArrayList<CardDetail> lst = new ArrayList<>();
            URL url = new URL(commonData.IP + CARD_SERVICE + GET_REMAINING_CARD);
            Client client = Client.create();
            WebResource res = client.resource(url.toString());
            ClientResponse cliRes = res.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

            String output = cliRes.getEntity(String.class);
            Gson gson = new Gson();
            lst = gson.fromJson(output, new TypeToken<ArrayList<CardDetail>>() {
            }.getType());

            return lst;
        } catch (MalformedURLException ex) {
            Logger.getLogger(CardService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    private static String getData(URL connection) throws IOException {

        String line = null;
        try {
            Client client = Client.create();
            WebResource res = client.resource(connection.toString());
            ClientResponse cliRes = res.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

            line = cliRes.getEntity(String.class);

            return line;

        } catch (ClientHandlerException | UniformInterfaceException e) {
            System.out.println(e);
        }

        return line;
    }

    public static Player reciveResult() {

        try {
            RoundResult cards = new RoundResult();
            int i = 1;
            for (CardDetail crd : lstCrdDetails) {
                if (i == 1) {
                    cards.setCard1(crd.getName());
                }
                if (i == 2) {
                    cards.setCard2(crd.getName());
                }
                if (i == 3) {
                    cards.setCard3(crd.getName());
                }
                if (i == 4) {
                    cards.setCard4(crd.getName());
                }
                if (i == 5) {
                    cards.setCard5(crd.getName());
                }
                i++;
            }
            Gson gson = new Gson();
            String outJson = gson.toJson(cards);
            
            String crdParam = "/"+cards.getCard1() + "/" + cards.getCard2() + "/"+ cards.getCard3() + "/"+cards.getCard4() + "/"+cards.getCard5();
            URL url = new URL(commonData.IP + SCORE_SERVICES + RECEIVE_HAND + crdParam + "/" + commonData.USERNAME + "/" + commonData.CURRENT_ROUND);
            //URL url = new URL(commonData.IP + SCORE_SERVICES + RECEIVE_HAND + "/" +outJson + "/" + commonData.USERNAME + "/" + commonData.CURRENT_ROUND);
            Client client = Client.create();
            WebResource webRes = client.resource(url.toString());

            ClientResponse cliRes = webRes.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            String out = cliRes.getEntity(String.class);
            JSONObject obj = new JSONObject(out);
            Player player = new Player();
            player.setRank(obj.getInt("rank"));
            player.setPoint(obj.getInt("points"));
            player.setRoundresult(obj.getInt("roundresult"));
            
            TOTAL += player.getRoundresult();

            return player;
        } catch (ClientHandlerException | UniformInterfaceException | MalformedURLException e) {
            System.out.println(e);
        }
        return null;
    }
}
