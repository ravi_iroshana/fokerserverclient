/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.entity;

import com.foker.common.PlayerStatus;

/**
 *
 * @author Ravindu
 */
public class Player{
    
    private String Username;
    private String Password;
    private String ConfirmPassword;
    private String Email;
    private PlayerStatus Status;
    private String game;
    private int roundresult;
    private int finelresult;
    private int Point;
    private int Rank;
    private int position; 

    
    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getConfirmPassword() {
        return ConfirmPassword;
    }

    public void setConfirmPassword(String ConfirmPassword) {
        this.ConfirmPassword = ConfirmPassword;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public PlayerStatus getStatus() {
        return Status;
    }

    public void setStatus(PlayerStatus Status) {
        this.Status = Status;
    }

    public int getRank() {
        return Rank;
    }

    public void setRank(int Rank) {
        this.Rank = Rank;
    }

    public int getPoint() {
        return Point;
    }

    public void setPoint(int Point) {
        this.Point = Point;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public int getRoundresult() {
        return roundresult;
    }

    public void setRoundresult(int roundresult) {
        this.roundresult = roundresult;
    }

    public int getFinelresult() {
        return finelresult;
    }

    public void setFinelresult(int finelresult) {
        this.finelresult = finelresult;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
