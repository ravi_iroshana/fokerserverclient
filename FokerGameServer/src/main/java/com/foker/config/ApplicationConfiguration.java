/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.config;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Ravindu
 */
@javax.ws.rs.ApplicationPath("WebResources")
public class ApplicationConfiguration extends Application{
    
    @Override
    public Set<Class<?>> getClasses(){
        Set<Class<?>> res = new HashSet<Class<?>>();
        addResResousesClass(res);
        return res;
    }

    private void addResResousesClass(Set<Class<?>> res) {
        res.add(com.foker.service.PlayerService.class);
        res.add(com.foker.service.CardServices.class);
        res.add(com.foker.service.BroadcastNotificationService.class);
        res.add(com.foker.service.ScoreServices.class);
    }
    
}
