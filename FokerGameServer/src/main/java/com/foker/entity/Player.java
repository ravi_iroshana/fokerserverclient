/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.entity;

import com.foker.common.PlayerStatus;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Ravindu
 */
public class Player implements Serializable, Comparable<Player>{

    
    private String Username;
    private String Password;
    private String ConfirmPassword;
    private String Email;
    private PlayerStatus Status;
   private String game;
    private int roundresult;
    private int finelresult;
    private int Points;
    private int Rank;
    private int position; 
    
    public Player(){}
    
    public Player(String Username, String Password, String Email){
        this.Username = Username;
        this.Password = Password;
        this.Email = Email;
    }
    
    @Override
    public int compareTo(Player anotherPlayer) {
        return this.getUsername().compareTo(anotherPlayer.getUsername());
    }
    
    @Override
    public int hashCode(){
        int hash = 5;
        hash = 25 * hash + (this.getUsername() != null ? this.getUsername().hashCode() : 0);
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Player other = (Player) obj;
        if (!Objects.equals(this.Username, other.Username)) {
            return false;
        }
        return true;
    }


    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public PlayerStatus getStatus() {
        return Status;
    }

    public void setStatus(PlayerStatus Status) {
        this.Status = Status;
    }

    public String getConfirmPassword() {
        return ConfirmPassword;
    }

    public void setConfirmPassword(String ConfirmPassword) {
        this.ConfirmPassword = ConfirmPassword;
    }

    /**
     * @return the game
     */
    public String getGame() {
        return game;
    }

    /**
     * @param game the game to set
     */
    public void setGame(String game) {
        this.game = game;
    }

    /**
     * @return the roundresult
     */
    public int getRoundresult() {
        return roundresult;
    }

    /**
     * @param roundresult the roundresult to set
     */
    public void setRoundresult(int roundresult) {
        this.roundresult = roundresult;
    }

    /**
     * @return the finelresult
     */
    public int getFinelresult() {
        return finelresult;
    }

    /**
     * @param finelresult the finelresult to set
     */
    public void setFinelresult(int finelresult) {
        this.finelresult = finelresult;
    }

    /**
     * @return the points
     */
    public int getPoints() {
        return Points;
    }

    /**
     * @param points the points to set
     */
    public void setPoints(int points) {
        this.Points = points;
    }

    /**
     * @return the position
     */
    public int getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * @return the Rank
     */
    public int getRank() {
        return Rank;
    }

    /**
     * @param Rank the Rank to set
     */
    public void setRank(int Rank) {
        this.Rank = Rank;
    }

    
}
