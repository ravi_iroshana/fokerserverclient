/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.entity;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author dmsadmin
 */
public class PokerHands {
    public static Map<String, Integer> handsvalues = new HashMap<String, Integer>();
    public PokerHands(){
        handsvalues.put("RoyalFlush", 180);
        handsvalues.put("StraightFlush", 170);
        handsvalues.put("FourofKind", 160);
        handsvalues.put("FullHouse", 150);
        handsvalues.put("Flush", 140);
        handsvalues.put("Straight", 130);
        handsvalues.put("ThreeofKind", 120);
        handsvalues.put("TwoPair", 110);
        handsvalues.put("Pair", 100);
        handsvalues.put("HighCard", 1);
    }
}
