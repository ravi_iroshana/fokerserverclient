/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.common;

import com.foker.entity.CardDetail;
import java.util.ArrayList;

/**
 *
 * @author Ravindu
 */
public class DeckOfCard {

    public final static String[] DECKOFCARD2 = {
        "CA", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "C10", "CJ", "CQ", "CK",
        "HA", "H1", "H2", "H3", "H4", "H5", "H6", "H7", "H8", "H9", "H10", "HJ", "HQ", "HK",
        "DA", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "D10", "DJ", "DQ", "DK",
        "SA", "S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10", "SJ", "SQ", "SK"
    };

    private final static String[] CARDFACE = {"C", "H", "D", "S"};

    public static ArrayList<CardDetail> DECKOFCARD = new ArrayList<CardDetail>();

    public static void setDeckOfCard() {
        CardDetail crdDetails;

        for (int i = 0; i < 4; i++) {
            for (int j = 2; j < 15; j++) {
                crdDetails = new CardDetail();
                crdDetails.setCardFace(CARDFACE[i]);
                crdDetails.setCardNo(String.valueOf(j));
                String face = String.valueOf(j);
                if (j == 11) {
                    face = "J";
                }
                if (j == 12) {
                    face = "Q";
                }
                if (j == 13) {
                    face = "K";
                }
                if (j == 14) {
                    face = "A";
                }
                crdDetails.setName(crdDetails.getCardFace() + face);
                crdDetails.setImgName(crdDetails.getName() + ".jpg");
                //crdDetails.setImgPath("file:./src/main/resources/images/deckofcard/" + crdDetails.getImgName());
                crdDetails.setImgPath("/images/deckofcard/" + crdDetails.getImgName());
                DECKOFCARD.add(crdDetails);
            }
        }
    }

}
