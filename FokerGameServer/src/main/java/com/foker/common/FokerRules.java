/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author thari
 */
public class FokerRules {
    char[] house = {'H','D','S','C'};
    static String[] fullhouse = {"A","K","Q","J","10"};
    
    int[] values = new int[5];
    public static String rule1(List<String> cards, char cardhouse){
        int i =0;
        String stat = null;
       int x = 0;
       int[] values = new int[5]; 
       if(cards.size() == 5){
          for(String card: cards){
              String val = card.substring(1);
                char cval = val.charAt(0);
              if(cval== fullhouse[0].charAt(0)){
                values[x] = 14;
                x++;
                i++;
              }else if(cval== fullhouse[1].charAt(0)){
                  values[x] = 13;
                  x++;
                  i++;
              }else if(cval== fullhouse[2].charAt(0)){
                  values[x] = 12;
                  x++;
                  i++;
              }else if(cval== fullhouse[3].charAt(0)){
                  values[x] = 11;
                  x++;
                  i++;
              }else if(cval== fullhouse[4].charAt(0)){
                  values[x] = 10;
                  x++;
                  i++;
              }else{
                  values[x] = Integer.valueOf(String.valueOf(card.charAt(1)));
                  x++;
              }
              }
          }
          if(i==5){
              stat = "RoyalFlush";
          }else{
              Arrays.sort(values);
              int firstval = values[0];
              int status =0;
              for(int k = 1;k<5;k++){
                  if(values[k] != (firstval+1)){
                      status = 1;
                  }
                  firstval = firstval+1;
              }
              if(status == 1){
                  stat = "Flush";
              }else{
                  stat = "StraightFlush";
              }
       }
       return stat;
}
    
    public static String rule2(List<String> cards){
        String hand = "";
        int x = 0;
       int[] values = new int[5]; 
        for(String card: cards){
              String val = card.substring(1);
                char cval = val.charAt(0);
              if(cval== fullhouse[0].charAt(0)){
                values[x] = 14;
                x++;
              }else if(cval== fullhouse[1].charAt(0)){
                  values[x] = 13;
                  x++;
              }else if(cval== fullhouse[2].charAt(0)){
                  values[x] = 12;
                  x++;
              }else if(cval== fullhouse[3].charAt(0)){
                  values[x] = 11;
                  x++;
              }else if(cval== fullhouse[4].charAt(0)){
                  values[x] = 10;
                  x++;
              }else{
                  values[x] = Integer.valueOf(String.valueOf(card.charAt(1)));
                  x++;
              }
            }
        int scount = 0;
        int pairs = 0;
        int trice = 0;
        int quartz =0;
        
        int straight = 0;
        Arrays.sort(values);
        int firstval = values[0];
        for(int a = 0;a<5;a++){
            if(a <= 3){
               if(values[a] == values[a+1]){
                   scount++;
                   if(scount == 1){
                       pairs++;
                   }else if(scount == 2){
                      pairs--;
                      trice++; 
                   }
                   else{
                      pairs--;
                      trice--;
                      quartz++; 
                   }
               }else{
                     scount = 0;
               }
               firstval++;
            }
        }
        if(quartz == 1){
            hand = "FourofKind";
        }else if(pairs == 1 && trice == 1){
            hand = "FullHouse";
        }else if(values[4] == firstval){
            hand = "Straight";
            pairs =0;
            trice = 0;
            quartz = 0;
        }
        else if(trice == 1 && pairs == 0 ){
            hand = "ThreeofKind";
        }else if(pairs == 2 && trice == 0){
            hand = "TwoPair";
        }else if(pairs == 1 && trice == 0){
            hand = "Pair";
        }else{
            hand = "HighCard";
        }
            
        return hand;
    }
    public static int rule3(List<String> cards){
       int x = 0;
       int tot = 0;
       int[] values = new int[5]; 
        for(String card: cards){
              String val = card.substring(1);
                char cval = val.charAt(0);
              if(cval== fullhouse[0].charAt(0)){
                values[x] = 14;
                x++;
              }else if(cval== fullhouse[1].charAt(0)){
                  values[x] = 13;
                  x++;
              }else if(cval== fullhouse[2].charAt(0)){
                  values[x] = 12;
                  x++;
              }else if(cval== fullhouse[3].charAt(0)){
                  values[x] = 11;
                  x++;
              }else if(cval== fullhouse[4].charAt(0)){
                  values[x] = 10;
                  x++;
              }else{
                  values[x] = Integer.valueOf(String.valueOf(card.charAt(1)));
                  x++;
              }
            }
        for(int i =0;i<=4;i++){
            tot = tot + values[i];
        }
        return tot;
    }
}
