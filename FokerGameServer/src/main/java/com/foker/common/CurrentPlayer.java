/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.common;

import static com.foker.common.Results.PLAYER_SIGNUP;
import com.foker.entity.Player;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ravindu
 */
public class CurrentPlayer {    
    
    public static int CURRENT_LEVEL = 0;   
    
    private static final List<Player> ALL_PLAYERS = new ArrayList<Player>();
    
    public static List<Player> getPlayers(){
        return ALL_PLAYERS;
    }
    
    public static String addPlayer(Player player){
        ALL_PLAYERS.add(player);
        return PLAYER_SIGNUP;
    }  

}
