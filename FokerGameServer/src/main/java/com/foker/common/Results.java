/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.common;

/**
 *
 * @author Ravindu
 */
public class Results {
    public static final String SUCSESS = "Success";
    public static final String ERROR = "Error";
    public static final String GAME_FAIL = "Fail";
    public static final String PLAYER_EXSIST = "Player Exsist";
    public static final String PLAYER_SIGNUP = "Player Signup";
    public static final String LOGIN_FAIL = "Login Fail";
}
