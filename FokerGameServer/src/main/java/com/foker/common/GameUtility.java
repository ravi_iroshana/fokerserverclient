/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.common;

import com.foker.entity.Player;

/**
 *
 * @author Ravindu
 */
public class GameUtility {
    
    public static Player findPlayer(String userName){
        for(Player player : CurrentPlayer.getPlayers()){
            if(player.getUsername().equals(userName)){
                return player;
            }
        }
        
        return null;
    }
}
