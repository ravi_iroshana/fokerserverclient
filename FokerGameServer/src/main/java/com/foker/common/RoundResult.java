/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.common;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thari
 */
public class RoundResult {
    private String card1;
    private String card2;
    private String card3;
    private String card4;
    private String card5;

    /**
     * @return the card1
     */
    public String getCard1() {
        return card1;
    }

    /**
     * @param card1 the card1 to set
     */
    public void setCard1(String card1) {
        this.card1 = card1;
    }

    /**
     * @return the card2
     */
    public String getCard2() {
        return card2;
    }

    /**
     * @param card2 the card2 to set
     */
    public void setCard2(String card2) {
        this.card2 = card2;
    }

    /**
     * @return the card3
     */
    public String getCard3() {
        return card3;
    }

    /**
     * @param card3 the card3 to set
     */
    public void setCard3(String card3) {
        this.card3 = card3;
    }

    /**
     * @return the card4
     */
    public String getCard4() {
        return card4;
    }

    /**
     * @param card4 the card4 to set
     */
    public void setCard4(String card4) {
        this.card4 = card4;
    }

    /**
     * @return the card5
     */
    public String getCard5() {
        return card5;
    }

    /**
     * @param card5 the card5 to set
     */
    public void setCard5(String card5) {
        this.card5 = card5;
    }

}
