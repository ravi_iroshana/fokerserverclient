/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.common;

import com.foker.entity.Player;
import com.foker.entity.PokerHands;
import com.foker.entity.RoundResult;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author thari
 */
public class RwinnerEvaluation {
    FokerRules rule = new FokerRules();
     public static Player winner(RoundResult cards,String name,int Round){
        PokerHands hands = new PokerHands();
        String stat = "fail";
        char[] house = {'H','D','S','C'};
        //add cards to array
        List<String> cardlist = new ArrayList<String>();
        cardlist.add(cards.getCard1());
        cardlist.add(cards.getCard2());
        cardlist.add(cards.getCard3());
        cardlist.add(cards.getCard4());
        cardlist.add(cards.getCard5());
        
        List<String> spades = new ArrayList<String>();
        List<String> harts = new ArrayList<String>();
        List<String> clubs = new ArrayList<String>();
        List<String> dimond = new ArrayList<String>();
        //deveide cards according to house
        for (String card : cardlist) {
            if(card.charAt(0) == house[0]){
                harts.add(card);
            }else if(card.charAt(0) == house[1]){
                dimond.add(card);
            }else if(card.charAt(0) == house[2]){
                spades.add(card);
            }else if(card.charAt(0) == house[3]){
                clubs.add(card);
            }
        }
        //check if all 5 cards in same house
        //if same house use rule 1
        //else use rule2 and 3 in foker
        String handname = "null";
        int tot;
        int handvalue = 0;
            if(harts.size()== 5){
                handname = FokerRules.rule1(harts,'H');
                handvalue = rPoints(handname);
            }else if(dimond.size()== 5){
                handname = FokerRules.rule1(dimond,'D');
                handvalue = rPoints(handname);
            }else if(spades.size()== 5){
                handname = FokerRules.rule1(spades,'S');
                handvalue = rPoints(handname);
            }else if(clubs.size()== 5){
                handname = FokerRules.rule1(clubs,'C');
                handvalue = rPoints(handname);
            }else{
                handname = FokerRules.rule2(cardlist);
                handvalue = rPoints(handname);
                if("HighCard".equals(handname)){
                    tot = FokerRules.rule3(cardlist);
                    handvalue = tot;
                }
            }
            Player player = updatePlayerData(name,handvalue);
            if(Round == 6){
                player = winningposition(player);
            }
            //Now need to evaluate points of all players in hear and return the place
        return player;
    }
     public static int rPoints(String hand){
         int val = PokerHands.handsvalues.get(hand);
         return val;
     }
     
          public static Player updatePlayerData(String name,int handvalue){
         List<Player> players = CurrentPlayer.getPlayers();
         Player play = null;
         for(Player player : players){
             String na = player.getUsername();
             if(na.equals(name)){
                 play = player;
                 play.setRoundresult(handvalue);
                 play.setFinelresult(player.getFinelresult() + player.getRoundresult());
             }
         }
         return play;
     }
     
     public static Player winningposition(Player name){
        List<Player> players = CurrentPlayer.getPlayers(); 
        Map<String, Integer> playerscores = new HashMap<String, Integer>();
        int[] scores = new int[players.size()+1];
        scores[0] = 50;
        int x = 1;
        int count = 0;
         for(Player player : players){
             if(player.getStatus() == PlayerStatus.ONLINE){
                 scores[x] = player.getFinelresult();
                 playerscores.put(player.getUsername(),player.getFinelresult());
                 count++;
             }
         }
         Arrays.sort(scores);
         int g =1;
         int position = count;
         for(int i =0;i < count;i++){
             for(int j =1;j < count +1;j++){
                 if(scores[i] == scores[j]){
                    for(Player player : players){
                        for(int b = 0;b < scores.length; b++){
                             if(player.getFinelresult() == scores[b]){
                                   player.setPosition(position);
                                   if(player.getFinelresult() > 1000){
                                       player.setPoints(player.getPoints()+100);
                                   }else if(player.getFinelresult() > 750){
                                       player.setPoints(player.getPoints()+75);
                                   }else if(player.getFinelresult() > 500){
                                       player.setPoints(player.getPoints()+50);
                                   }else if(player.getFinelresult() > 250){
                                       player.setPoints(player.getPoints()+25);
                                   }else{
                                       player.setPoints(player.getPoints()+10);
                                   }
                            }
                        }
                    } 
                 }else{
                     for(Player player : players){
                         for(int b = 0;b< scores.length;b++){
                            if(player.getFinelresult() == scores[b]){
                                   player.setPosition(position);
                                   position--;
                                   if(player.getFinelresult() > 1000){
                                       player.setPoints(player.getPoints()+100);
                                   }else if(player.getFinelresult() > 750){
                                       player.setPoints(player.getPoints()+75);
                                   }else if(player.getFinelresult() > 500){
                                       player.setPoints(player.getPoints()+50);
                                   }else if(player.getFinelresult() > 250){
                                       player.setPoints(player.getPoints()+25);
                                   }else{
                                       player.setPoints(player.getPoints()+10);
                                   }
                            }
                         }
                             
                    }
                 }
         }
         }
         Player winposition = null;
         for(Player player : players){
            if(player.getUsername().equals(name.getUsername())){
                if(player.getPoints() > 200){
                                player.setRank(player.getRank()+1);
                                player.setPoints(player.getPoints()- 200);
                                player.setFinelresult(0);
                                player.setRoundresult(0);
                            }
                     winposition = player;              
            }
          } 
         
         return winposition;
     }
}
