/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.common;

/**
 *
 * @author Ravindu
 */
public enum PlayerStatus {
    ONLINE,
    PLAYING,
    JOIN,
    COMPLETE_HAND,
    ROUND1_COMPLETE,
    ROUND2_COMPLETE,
    ROUND3_COMPLETE,
    ROUND4_COMPLETE,
    ROUND5_COMPLETE,
    ROUND6_COMPLETE
}
