/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.service;

import com.foker.common.CurrentPlayer;
import com.foker.common.GameUtility;
import com.foker.common.PlayerStatus;
import static com.foker.common.PlayerStatus.ONLINE;
import static com.foker.common.Results.ERROR;
import static com.foker.common.Results.LOGIN_FAIL;
import static com.foker.common.Results.PLAYER_EXSIST;
import static com.foker.common.Results.SUCSESS;
import com.foker.entity.Player;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Ravindu
 */
@Path("/PlayerService")
public class PlayerService {

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/loginPlayer/{userName}/{Password}")
    public String loginPlayer(@PathParam("userName") String userName, @PathParam("Password") String Password) {

        for (Player plyr : CurrentPlayer.getPlayers()) {
            if (userName.equals(plyr.getUsername()) && Password.equals(plyr.getPassword())) {
                plyr.setStatus(ONLINE);
                return SUCSESS;
            }
        }
        return LOGIN_FAIL;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/registerPlayer/{userName}/{password}/{email}")
    public String registerPlayer(@PathParam("userName") String userName, @PathParam("password") String password, @PathParam("email") String email) {
        Player player = new Player(userName, password, email);

        List<Player> lst = CurrentPlayer.getPlayers();
        boolean isExsist = lst.contains(player);

        if (isExsist) {
            return PLAYER_EXSIST;
        }

        String signUP = CurrentPlayer.addPlayer(player);
        if (signUP.equals("Player Signup")) {
            try {
                saveUserData(player);
            } catch (IOException ex) {
                Logger.getLogger(PlayerService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return SUCSESS;
        }

        return ERROR;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getJoinPlayer")
    public List<Player> getJoinPlayer() {
        List<Player> lst = new ArrayList<>();
        for (Player ply : CurrentPlayer.getPlayers()) {
            if (ply.getStatus().equals(PlayerStatus.ONLINE)) {
                lst.add(ply);
            }
        }

        return lst;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/GetPlayerinfo/{player}")
    public Player getPlayerInfo(@PathParam("player") String name) {
    List<Player> players = CurrentPlayer.getPlayers();
    Player play = null;
    for(Player player : players){
             String na = player.getUsername();
             if(na.equals(name)){
                 play = player;
             }
         }
        return play;
    }
    
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/removePLayer")
    public String removePlayer() {

        return null;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getData/{name1}/{name2}/{name3}")
    public String getData(@PathParam("name1") String name1, @PathParam("name2") String name2, @PathParam("name3") String name3) {
        return name1 + name2 + "- -" + name3;
    }

    private void saveUserData(Player player) throws IOException {
        String path = "./txtFile/userData.txt";

        FileWriter fileWriter = new FileWriter(path, true);
        try (PrintWriter printWriter = new PrintWriter(fileWriter)) {
            printWriter.println(player);
        }
    }

}
