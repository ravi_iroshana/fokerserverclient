/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.service;

import com.foker.common.CurrentPlayer;
import com.foker.common.PlayerStatus;
import static com.foker.common.PlayerStatus.ONLINE;
import static com.foker.common.Results.SUCSESS;
import com.foker.entity.Player;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.media.sse.SseBroadcaster;
import org.glassfish.jersey.media.sse.SseFeature;

/**
 *
 * @author Ravindu
 */

@Singleton
@Path("/BroadcastNotificationService")
public class BroadcastNotificationService {
    private final SseBroadcaster sseBroadCaster = new SseBroadcaster();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/getBroadcastJoinPlayer/{message}")
    public String getBroadcastJoinPlayer(@PathParam("message") String message){
        System.out.println("******************************");
        OutboundEvent.Builder outBoundEvt = new OutboundEvent.Builder();
        OutboundEvent evt = outBoundEvt.name("Join")
                .mediaType(MediaType.TEXT_PLAIN_TYPE)
                .data(String.class, message)
                .build();
        
        sseBroadCaster.broadcast(evt);
        
        return "Your are Join";                
    }
    
    @GET
    @Produces(SseFeature.SERVER_SENT_EVENTS)
    @Path("/listenJoinPlayer")
    public EventOutput listenJoinPlayer(){
        EventOutput evtOut = new EventOutput();
        this.sseBroadCaster.add(sseBroadCaster);
        return evtOut;
    }
    
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/roundCompletion/{username}")
    public String roundCompletion(@PathParam("username") String username){
        Player player = new Player();
        for (Player plyr : CurrentPlayer.getPlayers()) {
            if (username.equals(plyr.getUsername())) {                
                plyr.setStatus(PlayerStatus.ROUND1_COMPLETE);  
                player = plyr;
            }
        }
        OutboundEvent.Builder outBoundEvt = new OutboundEvent.Builder();
        OutboundEvent evt = outBoundEvt.name("complete")
                .mediaType(MediaType.APPLICATION_JSON_TYPE)
                .data(String.class,player.getUsername() + "@" + player.getStatus() + "@")
                .build();
        
        sseBroadCaster.broadcast(evt);
                       
        return "Round Complete";
    }
    
    
}
