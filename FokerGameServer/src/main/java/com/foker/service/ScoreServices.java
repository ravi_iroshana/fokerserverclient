/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.service;

import com.foker.entity.RoundResult;
import com.foker.common.RwinnerEvaluation;
import com.foker.entity.Player;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Ravindu
 */

@Path("/ScoreServices")
public class ScoreServices {
    RwinnerEvaluation win = new RwinnerEvaluation();
    
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/receiveHand/{card1}/{card2}/{card3}/{card4}/{card5}/{player}/{round}")
    public Player receiveHand(@PathParam("card1") String card1,@PathParam("card2") String card2,@PathParam("card3") String card3,
            @PathParam("card4") String card4,@PathParam("card5") String card5,@PathParam("player") String player,
            @PathParam("round") String round) {
        
        RoundResult cards = new RoundResult();
        cards.setCard1(card1);
        cards.setCard2(card2);
        cards.setCard3(card3);
        cards.setCard4(card4);
        cards.setCard5(card5);
        
        Player play = RwinnerEvaluation.winner(cards,player,Integer.valueOf(round));
        return play;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/gethand")
    public int receivehand1() {
        String card = "H4";
        int x = Integer.valueOf(String.valueOf(card.charAt(1)));
        return x;
    }
}
