/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foker.service;

import com.foker.common.DeckOfCard;
import static com.foker.common.DeckOfCard.DECKOFCARD;
import com.foker.entity.CardDetail;
import java.util.ArrayList;
import java.util.Random;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Ravindu
 */
@Path("/CardServices")
public class CardServices {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/getInitialCard")
    public ArrayList<CardDetail> getInitialCard() {
        Random random = new Random();
        DeckOfCard.setDeckOfCard();
        ArrayList<CardDetail> initialCard = new ArrayList<>();

        synchronized (CardServices.class) {
            int i = 0;
            while (i < 2) {
                int card = random.nextInt(DECKOFCARD.size());                
                initialCard.add(DECKOFCARD.get(card));
                DECKOFCARD.remove(card);
                i++;
            }
        }

        return initialCard;
    }
    
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getRemainingCards")
    public ArrayList<CardDetail> getRemainingCard(){
        Random random = new Random();
        
        ArrayList<CardDetail> crdlst = new ArrayList<>();
        
        synchronized(CardServices.class){
            int i =0;
            while(i < 3){
                int card = random.nextInt(DECKOFCARD.size());
                crdlst.add(DECKOFCARD.get(card));
                DECKOFCARD.remove(card);
                i++;
            }
        }
        
        return crdlst;
    }
    
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getChangesCard/{card1}/{card2}/{card3}")
    public ArrayList<CardDetail> getChangestCard(@PathParam("card1") String card1,@PathParam("card2") String card2,@PathParam("card3") String card3){
        ArrayList<CardDetail> lst = new ArrayList<>();
        
        Random random = new Random();
        CardDetail crd = new CardDetail();
        
        synchronized(CardServices.class){
            int i = 0;
            boolean isDifferent = true;
            while(i < 3){
                int card = random.nextInt(DECKOFCARD.size());
                crd = DECKOFCARD.get(card);
                if(crd.getName().equals(card1) || crd.getName().equals(card2) || crd.getName().equals(card3)){                     
                    isDifferent = false;
                }
                
                if(isDifferent){
                    lst.add(crd);
                    DECKOFCARD.remove(card);
                    i++;
                }                
            }
        }
        
        return lst;
    }
    

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get")
    public String getData(String a) {
        return a;
    }

}
